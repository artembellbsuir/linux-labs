#include <stdio.h>
#include <stdlib.h>

void main(int argc, char *args[]) {
    if (argc - 1 != 2) {
        printf("Пожалуйста, введите 2 целочисленных параметра");
    } else {
        int param1 = atoi(args[1]);
        int param2 = atoi(args[2]);

        srand(param1);
        int n = rand() % param2 + 1;

        printf("Номер Вашего варианта: %d\n", n);
    }
}
